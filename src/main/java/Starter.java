import nl.delphinity.jas.domain.Brood;
import nl.delphinity.jas.domain.Brooddoos;
import nl.delphinity.jas.persistence.factory.DAOFactories;
import nl.delphinity.jas.persistence.factory.DAOFactory;
import nl.delphinity.jas.persistence.service.BroodService;
import nl.delphinity.jas.persistence.service.BrooddoosService;
import nl.delphinity.jas.persistence.utils.HibernateSessionManager;



public class Starter {

	public static void main(String[] args) {
	
		DAOFactory.setTheFactory(DAOFactories.HIBERNATE.getTheFactory());
		
		Brooddoos brooddoos = new Brooddoos("Giorgio's Brooddoos");
		Brood volkoren = new Brood("volkoren");
		Brood meergranen = new Brood("meergranen");
		Brood spelt = new Brood("spelt");
		
		brooddoos.addBrood(volkoren);
		brooddoos.addBrood(meergranen);
		brooddoos.addBrood(spelt);
		
	

		try {
			BrooddoosService.getInstance().createBrooddoos(brooddoos);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		try {
			BroodService.getInstance().createBrood(volkoren);
			BroodService.getInstance().createBrood(meergranen);
			BroodService.getInstance().createBrood(spelt);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		HibernateSessionManager.shutdown();

	}

}
