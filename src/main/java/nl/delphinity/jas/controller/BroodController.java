package nl.delphinity.jas.controller;

import com.opensymphony.xwork2.ActionSupport;

import nl.delphinity.jas.domain.Brood;
import nl.delphinity.jas.persistence.service.BroodService;

public class BroodController extends ActionSupport {
	
	public String soort;
	private BroodService broodservice = BroodService.getInstance();
	
	public String execute() {
		return SUCCESS;
	}
	
	public String save() {
		Brood brood = new Brood();
		brood.setSoort(soort);
		try {
			broodservice.createBrood(brood);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return SUCCESS;
	}

	public String getSoort() {
		return soort;
	}

	public void setSoort(String soort) {
		this.soort = soort;
	}
}
