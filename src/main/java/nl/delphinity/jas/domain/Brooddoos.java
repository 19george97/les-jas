package nl.delphinity.jas.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import antlr.collections.List;
import edu.emory.mathcs.backport.java.util.TreeSet;

@Entity
@Table(name = "brooddoos")
public class Brooddoos implements Comparable<Brooddoos> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int ID;

	@Column(name = "name")
	private String name;

	@OneToMany(mappedBy = "brooddoos")
	private Set<Brood> inhoudbrooddoos = new HashSet<Brood>();

	// constructor
	public Brooddoos() {

	}

	public Brooddoos(String name) {
		super();
		this.name = name;
	}

	// getters and setters

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addBrood(Brood b) {
		this.inhoudbrooddoos.add(b);
		b.setBrooddoos(this);
		}

	@Override
	public int compareTo(Brooddoos o) {
		return name.compareTo(o.getName());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Brooddoos other = (Brooddoos) obj;
		if (ID != other.ID)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Brooddoos [ID=" + ID + ", name=" + name + "]";
	}

}
