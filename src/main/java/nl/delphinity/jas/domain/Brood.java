package nl.delphinity.jas.domain;

import javax.persistence.*;

@Entity
@Table(name = "brood")
public class Brood implements Comparable<Brood> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "brood_ID")
	private int ID;

	@Column(name = "brood_soort")
	private String soort;

//	@Column(name = "brood_beleg")
//	private Beleg beleg;

	@ManyToOne
	@JoinColumn (name = "brooddoos", foreignKey = @ForeignKey(name = "brooddoos_ID_FK"))
	private Brooddoos brooddoos;

	public Brood() {

	}

	public Brood(String soort) {
		super();
		this.soort = soort;
	}

	// getters and setters

	public String getSoort() {
		return soort;
	}

	public void setSoort(String name) {
		this.soort = name;
	}

//	public Beleg getBeleg() {
//		return beleg;
//	}
//
//	public void setBeleg(Beleg beleg) {
//		this.beleg = beleg;
//	}

	public Brooddoos getBrooddoos() {
		return brooddoos;
	}

	public void setBrooddoos(Brooddoos brooddoos) {
		this.brooddoos = brooddoos;
	}

	@Override
	public int compareTo(Brood o) {
		return soort.compareTo(o.getSoort());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Brood other = (Brood) obj;
		if (ID != other.ID)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Brood [ID=" + ID + ", soort=" + soort + "]";
	}

}
