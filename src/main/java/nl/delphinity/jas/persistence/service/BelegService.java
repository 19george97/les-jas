package nl.delphinity.jas.persistence.service;

import java.util.Collection;
import java.util.Set;

import com.opensymphony.xwork2.ActionSupport;

import nl.delphinity.jas.domain.Beleg;
import nl.delphinity.jas.persistence.factory.DAOFactory;

public class BelegService extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private static BelegService instance = null;

	public static BelegService getInstance() {
		if (instance == null) {
			instance = new BelegService();
		}
		return instance;
	}

	public static void setInstance(BelegService instance) {
		BelegService.instance = instance;
	}

	public void createBeleg(Beleg t) throws Exception {
		DAOFactory.getTheFactory().getBelegDAO().saveOrUpdate(t);

	}
	
	public void subscribeToBeleg(Beleg t) {
		DAOFactory.getTheFactory().getBelegDAO().saveOrUpdate(t);
	}

	public void deleteBeleg(Beleg t) {

		DAOFactory.getTheFactory().getBelegDAO().delete(t);

	}
	public Beleg findBelegById(int Id) {

		return DAOFactory.getTheFactory().getBelegDAO().findById(Id);

	}

	public void updateBeleg(Beleg t) throws Exception {

		DAOFactory.getTheFactory().getBelegDAO().saveOrUpdate(t);

	}

	public Collection<Beleg> readAllBelegs() {

		return DAOFactory.getTheFactory().getBelegDAO().findAll();

	}
	public Set<Beleg> readAll() {

		return DAOFactory.getTheFactory().getBelegDAO().findAll();

	}

}