package nl.delphinity.jas.persistence.service;

import java.util.Collection;
import java.util.Set;

import com.opensymphony.xwork2.ActionSupport;

import nl.delphinity.jas.domain.Brood;
import nl.delphinity.jas.persistence.factory.DAOFactory;

public class BroodService extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private static BroodService instance = null;

	public static BroodService getInstance() {
		if (instance == null) {
			instance = new BroodService();
		}
		return instance;
	}

	public static void setInstance(BroodService instance) {
		BroodService.instance = instance;
	}

	public void createBrood(Brood t) throws Exception {
		DAOFactory.getTheFactory().getBroodDAO().saveOrUpdate(t);

	}
	
	public void subscribeToBrood(Brood t) {
		DAOFactory.getTheFactory().getBroodDAO().saveOrUpdate(t);
	}

	public void deleteBrood(Brood t) {

		DAOFactory.getTheFactory().getBroodDAO().delete(t);

	}
	public Brood findBroodById(int Id) {

		return DAOFactory.getTheFactory().getBroodDAO().findById(Id);

	}

	public void updateBrood(Brood t) throws Exception {

		DAOFactory.getTheFactory().getBroodDAO().saveOrUpdate(t);

	}

	public Collection<Brood> readAllBroods() {

		return DAOFactory.getTheFactory().getBroodDAO().findAll();

	}
	public Set<Brood> readAll() {

		return DAOFactory.getTheFactory().getBroodDAO().findAll();

	}

}