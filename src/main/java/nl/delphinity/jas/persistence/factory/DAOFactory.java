package nl.delphinity.jas.persistence.factory;


import nl.delphinity.jas.persistence.interfaces.IBrooddoosDAO;
import nl.delphinity.jas.persistence.interfaces.IBroodDAO;
import nl.delphinity.jas.persistence.interfaces.IBelegDAO;


public abstract class DAOFactory {
	
	private static DAOFactory theFactory;

	/**
	 * @return the theFactory
	 */
	public static DAOFactory getTheFactory() {
		return theFactory;
	}
	

	/**
	 * @param factory the theFactory to set
	 */
	public static void setTheFactory(Class<? extends DAOFactory> factory) {
		try {
			theFactory = factory.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to create DAOFactory: " + factory);
		}
	}
	
	public abstract IBrooddoosDAO getBrooddoosDAO();
	public abstract IBroodDAO getBroodDAO();
	public abstract IBelegDAO getBelegDAO();

}