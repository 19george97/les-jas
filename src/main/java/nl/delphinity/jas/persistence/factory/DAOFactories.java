package nl.delphinity.jas.persistence.factory;

/**
 * @author jarnob
 * @nakedvos
 * @since 1.0
 * @20 mrt. 2019
 */

public enum DAOFactories {

	HIBERNATE(HibernateDAOfactory.class);
	
	private final Class<? extends DAOFactory> theFactory;
	
	private DAOFactories(Class<? extends DAOFactory> factory) {
		this.theFactory = factory;
	}

	/**
	 * @return the theFactory
	 */
	public Class<? extends DAOFactory> getTheFactory() {
		return theFactory;
	}
	
	
	
}
