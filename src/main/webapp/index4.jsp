<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<title>AO JAS</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>

	<div class="container">

		<div class="row">
			<div class="col-12">
				<h2>Toon een modal wanneer ik op Q druk</h2>
			</div>
		</div>
		<br /> <br /> <br />

		<div class="row">
			<div class="col-12">
			
			<button class="btn btn-primary" id="showmodalbutton">Toon modal</button>

				<div class="modal" tabindex="-1" role="dialog" id="">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Nieuwe boterham toevoegen</h5>
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form action="savebrood4" method="POST">
								<div class="modal-body">
									<div class="form-group row">
										<label class="col-sm-4">Naam:</label>
										<div class="col-sm-8">
											<s:textfield class="form-control" name="soort" id="name"></s:textfield>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<s:submit class="btn btn-primary" value="Opslaan">
									</s:submit>
									<button type="button" class="btn btn-secondary"
										data-dismiss="modal">Close</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>

	<script src="js/app4.js"></script>
</body>
</html>